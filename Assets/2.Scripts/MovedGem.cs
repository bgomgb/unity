﻿using System;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class MovedGem : MonoBehaviour
{
    private static MovedGem instance;
    public bool bIsMoveGem = false;
    public static MovedGem Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<MovedGem>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("MovedGem").AddComponent<MovedGem>();
                    instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    public IEnumerator MovedGems(Gem gem1,Gem gem2)
    {
        Gem cgem1  =Instantiate(gem1, gem1.transform.parent);
        Gem cgem2 = Instantiate(gem2, gem2.transform.parent);
        
        gem1.GetComponent<Image>().enabled = false;
        gem2.GetComponent<Image>().enabled = false;

        cgem1.transform.DOMove(gem2.transform.position, 0.25f);
        cgem2.transform.DOMove(gem1.transform.position, 0.25f);

        yield return new WaitForSeconds(0.25f);

        Destroy(cgem1.gameObject);
        Destroy(cgem2.gameObject);

        SwapGem(gem1, gem2);
        gem1.GetComponent<Image>().enabled = true;
        gem2.GetComponent<Image>().enabled = true;

        StageManager.Instance.SelectGem = null;

    }

    public void SwapGem(Gem gem1, Gem gem2)
    {
        GameDefine.GemGode tempCode = gem1.mCode;
        gem1.mCode = gem2.mCode;
        gem2.mCode = tempCode;

        gem1.setResize();
        gem2.setResize();

    }

}

