﻿using UnityEngine;
using UnityEditor;

public interface IState
{
     void OnEnter();
     void Update();
     void OnExit(); 
}