﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

public class LineCheck 
{
    public List<Gem> RemoveGem = new List<Gem>();

    private bool CheckMatchHorizon(Gem gem, int MatchCount)
    {
        List<Gem> ListMatchHorizon = new List<Gem>();
        int CheckPointX = MatchCount * -1 + 1;

        for (int i = 0; i < MatchCount * 2 - 1; i++)
        {
            int iX = gem.X + CheckPointX;
            Gem checkGem = StageManager.Instance.getGem(iX, gem.Y);

            if (checkGem == null || checkGem.mCode != gem.mCode)
            {
                if (ListMatchHorizon.Count < 3)
                    ListMatchHorizon.Clear();
            }
            else
            {
                if (ListMatchHorizon.Count != 0)
                {
                    if (ListMatchHorizon[ListMatchHorizon.Count - 1].X == checkGem.X - 1)
                        ListMatchHorizon.Add(checkGem);

                }
                else
                    ListMatchHorizon.Add(checkGem);

            }

            ++CheckPointX;
        }

        if (ListMatchHorizon.Count < MatchCount)
            ListMatchHorizon.Clear();
        else
        {
            foreach (var member in ListMatchHorizon)
                RemoveGem.Add(member);
        }


        return true;
    }

    private bool CheckMatchVetical(Gem gem, int MatchCount)
    {
        List<Gem> ListMatchVetical = new List<Gem>();
        int CheckPointY = MatchCount * -1 + 1;

        for (int i = 0; i < MatchCount * 2 - 1; i++)
        {
            int iY = gem.Y + CheckPointY;
            Gem checkGem = StageManager.Instance.getGem(gem.X, iY);

            if (checkGem == null || checkGem.mCode != gem.mCode)
            {
                if(ListMatchVetical.Count<3)
                    ListMatchVetical.Clear();
            }
            else
            {

                if(ListMatchVetical.Count != 0)
                {                    
                    if(ListMatchVetical[ListMatchVetical.Count - 1].Y== checkGem.Y-1)
                        ListMatchVetical.Add(checkGem);

                }
                else
                    ListMatchVetical.Add(checkGem);
            }

            ++CheckPointY;
        }

        if (ListMatchVetical.Count < MatchCount)
            ListMatchVetical.Clear();
        else
        {
            foreach (var member in ListMatchVetical)
                RemoveGem.Add(member);
        }

        return true;
    }


    public void FindLineCheak(Gem gem)
    {
        RemoveGem.Clear();

        CheckMatchHorizon(gem, GameDefine.MatchCount);
        CheckMatchVetical(gem, GameDefine.MatchCount);
    }
   
    public int CheckCount()
    {
        return RemoveGem.Count;
    }
 
}