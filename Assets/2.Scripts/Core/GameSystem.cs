﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class GameSystem 
{
    public LineCheck matchLineCheck = new LineCheck();

    List<Gem> RemoveGem = new List<Gem>();
    List<Gem> ModifyGems = new List<Gem>();


    public IEnumerator GemMove(Gem Cgem)
    {

        yield return moveLineCheak(Cgem);

        yield return null;
    }

    bool LinefillDirection = false;
    public IEnumerator moveLineCheak(Gem Cgem)
    {
        MovedGem.Instance.bIsMoveGem = true;

        Gem SeletGem = StageManager.Instance.SelectGem;
        Gem MoveGem = Cgem;
        yield return MovedGem.Instance.MovedGems(StageManager.Instance.SelectGem, Cgem);

        matchLineCheck.FindLineCheak(SeletGem);

        if(matchLineCheck.CheckCount()>0)
        {
            foreach(var gem in matchLineCheck.RemoveGem)
            {
                RemoveGem.Add(gem);
            }
        }

        matchLineCheck.FindLineCheak(MoveGem);
        if (matchLineCheck.CheckCount() > 0)
        {
            foreach (var gem in matchLineCheck.RemoveGem)
            {
                RemoveGem.Add(gem);
            }
        }

        if (RemoveGem.Count > 0)
        {
            yield return explosionGem();

            yield return LineFill();            
        }
        else
         yield return MovedGem.Instance.MovedGems(SeletGem, MoveGem);

        RemoveGem.Clear();

        yield return new WaitUntil(() => LinefillDirection != true);

        yield return ModifyLineCheck();

        MovedGem.Instance.bIsMoveGem = false;


    }

    public IEnumerator ModifyLineCheck()
    {
        Debug.Log("ModifyLineCheck");

        foreach(var gem in ModifyGems)
        {
            matchLineCheck.FindLineCheak(gem);

            foreach (var rmgem in matchLineCheck.RemoveGem)
            {
                RemoveGem.Add(rmgem);
            }
        }

        ModifyGems.Clear();

        if (RemoveGem.Count > 0)
        {
            yield return explosionGem();

            yield return LineFill();
        }

        RemoveGem.Clear();

        if (ModifyGems.Count != 0)
           yield return ModifyLineCheck();


        yield return null;
    }


    class DirectionMoveGem
    {
        public Gem StartGem;
        public Gem EndGem;
        public GameDefine.GemGode Start;
        public int EmptyCount;
    }

    List<DirectionMoveGem> dirList = new List<DirectionMoveGem>();


    public IEnumerator LineFill()
    {
        //라인을 체크하고 
        //라인을 내리자
        
        for (int i=0; i<StageManager.Instance.mWidth; i++)
        {
            int emptyCount = 1;
            for(int sY= StageManager.Instance.mHeight-1; sY>=0; sY--)
            {

                var startGem = StageManager.Instance.getGem(i,sY);

                if (startGem.mCode != GameDefine.GemGode.None)
                    continue;

                for(int cY = sY - 1; cY >= 0; cY--)
                {
                    var CheckGem = StageManager.Instance.getGem(i, cY);

                    if (CheckGem.mCode != GameDefine.GemGode.None)
                    {
                        DirectionMoveGem addGem = new DirectionMoveGem();
                        addGem.StartGem = CheckGem;
                        addGem.EndGem = startGem;
                        addGem.Start = CheckGem.mCode;

                        startGem.mCode = CheckGem.mCode;
                        CheckGem.mCode = GameDefine.GemGode.None;

                        dirList.Add(addGem);
                        //startGem.setResize();
                        //CheckGem.setResize();
                        break;
                    }                          
                }

                if(startGem.mCode == GameDefine.GemGode.None)
                {

                    DirectionMoveGem addGem = new DirectionMoveGem();
                    addGem.StartGem = null;
                    addGem.EndGem =  startGem;
                    addGem.EmptyCount = emptyCount;
                    startGem.mCode = (GameDefine.GemGode)UnityEngine.Random.Range(1, (int)GameDefine.GemGode.Count);
                    addGem.Start = startGem.mCode;
                     dirList.Add(addGem);

                    ++emptyCount;
                    //startGem.setResize();
                }

                ModifyGems.Add(startGem);
            }
        }
        yield return LineFillDirection();

        yield return null;
    }

    public IEnumerator LineFillDirection()
    {
        LinefillDirection = true;

        List<Gem> destoryObject =new List<Gem>();
        foreach(var dir in dirList)
        {
            Gem moveGem;

            if(dir.StartGem == null)
            {
                var LineStartGem  = StageManager.Instance.getGem(dir.EndGem.X, 0);

                moveGem = GameObject.Instantiate(LineStartGem, LineStartGem.transform.parent);
                moveGem.transform.localPosition = new Vector3(moveGem.transform.localPosition.x, moveGem.transform.localPosition.y+ moveGem.GetComponent<RectTransform>().rect.height*dir.EmptyCount, moveGem.transform.localPosition.z);
                moveGem.mCode = dir.Start;
                moveGem.setResize();


            }
            else
            {
                moveGem = GameObject.Instantiate(dir.StartGem, dir.StartGem.transform.parent);
                moveGem.mCode = dir.Start;
                moveGem.setResize();
            }

            if (dir.StartGem != null)
                dir.StartGem.GetComponent<Image>().enabled = false;

            if (dir.EndGem != null)
                dir.EndGem.GetComponent<Image>().enabled = false;

            moveGem.transform.DOMove(dir.EndGem.transform.position, 0.5f);

            destoryObject.Add(moveGem);        

        }

        yield return new WaitForSeconds(0.6f);

        foreach(var dobj in destoryObject)
        {
            GameObject.Destroy(dobj.gameObject);
        }

        foreach (var dir in dirList)
        {
            if (dir.StartGem != null)
                dir.StartGem.setResize();

            if (dir.EndGem != null)
                dir.EndGem.setResize();
        }

           dirList.Clear();
        LinefillDirection = false;
        yield return null;
    }

    public IEnumerator explosionGem()
    {

        foreach(var gem in RemoveGem)
        {
            gem.Explosion();
        }

        yield return null;

    }
}