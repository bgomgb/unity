﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gem : MonoBehaviour
{
    public GameDefine.GemGode mCode;
    public int X, Y;    

    public void setResize()
    {
        var Image= GetComponent<Image>();

        if(mCode == GameDefine.GemGode.None)
        {
            Image.enabled = false;
        }
        else
         {
           Image.enabled = true;
           Image.sprite = Resources.Load<Sprite>(mCode.ToString());
           Image.SetNativeSize();
        }
        
    }

    public void Explosion()
    {
         GetComponent<Image>().enabled = false;
         mCode = GameDefine.GemGode.None;
    }

    public bool checkMoveGem(Gem MoveGem)
    {
        //위아래왼쪽오른쪽 이동가능
        if(this.X== MoveGem.X)
        {
            if (this.Y == MoveGem.Y - 1)
                return true;

            else if(this.Y == MoveGem.Y + 1)
                return true;
        }

        if (this.Y == MoveGem.Y)
        {
            if (this.X == MoveGem.X - 1)
                return true;

            else if (this.X == MoveGem.X + 1)
                return true;
        }

        return false;
    }
}
