﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

public class StageManager : MonoBehaviour
{
    private static StageManager instance;
    public GameObject[,] arrayMap;

    public int mWidth, mHeight;
    private GameObject Gems;

    public bool IsMoveGem = false;

    public Gem SelectGem;

    public GameSystem gamesystem =new GameSystem();
     

    public static StageManager Instance
    {
        get
        {
            if (instance == null)
            {
                var obj = FindObjectOfType<StageManager>();

                if (obj != null)
                {
                    instance = obj;
                }
                else
                {
                    var newSingleton = new GameObject("Stage Manager").AddComponent<StageManager>();
                    instance = newSingleton;
                }
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<StageManager>();

        if (objs.Length != 1)
        {

            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

    }

    public void CreateStage(string stage)
    {
        List<Dictionary<string, object>> data = CSVReader.Read(stage);

        mWidth = data.Count;
        mHeight = data[0].Count;

        arrayMap = new GameObject[mHeight, mWidth];
        Gems = GameObject.Find("Gems");

        if (Gems == null)
        {
            Debug.Log("Gems 오브젝트를 찾을수 없다");
            return;
        }

        for (var i = 0; i < mWidth; i++)
        {
            for (var j = 0; j < mHeight; j++)
            {
                CreateObjcet(i, j, data[i]["Line" + $"{j + 1}"].ToString());
            }
        }
        IsMoveGem = false;
    }

    private void CreateObjcet(int X, int Y, string Code)
    {
        GameDefine.GemGode objectCode = objectCode = GameDefine.GemGode.None;

        switch (Code)
        {
            case "G":
                objectCode = GameDefine.GemGode.Gem_Green;
                break;
            case "B":
                objectCode = GameDefine.GemGode.Gem_Blue;
                break;
            case "P":
                objectCode = GameDefine.GemGode.Gem_Purple;
                break;
            case "O":
                objectCode = GameDefine.GemGode.Gem_Orange;
                break;
            case "Y":
                objectCode = GameDefine.GemGode.Gem_Yellow;
                break;
            case "R":
                objectCode = GameDefine.GemGode.Gem_Red;
                break;
            case "W":
                objectCode = GameDefine.GemGode.Gem_White;
                break;
        }

        arrayMap[X, Y] = new GameObject($"{X}_{Y}");
        arrayMap[X, Y].transform.parent = Gems.transform;

        var gWidth = (Gems.GetComponent<RectTransform>().sizeDelta.x - (100 * mWidth)) / mWidth;
        var gheight = (Gems.GetComponent<RectTransform>().sizeDelta.y - (100 * mHeight)) / mHeight;
        if (objectCode != GameDefine.GemGode.None)
        {
            var obj = arrayMap[X, Y].AddComponent<Gem>();
            arrayMap[X, Y].AddComponent<Image>();
            arrayMap[X, Y].AddComponent<Drag>();

            obj.tag = "Gem";
            obj.mCode = objectCode;
            obj.X = X;
            obj.Y = Y;
            arrayMap[X, Y].transform.localPosition = new Vector3((gWidth + 100) * X, -((gheight + 100) * Y));
            arrayMap[X, Y].transform.localPosition += new Vector3(58.2f, -59.9f);
            arrayMap[X, Y].transform.localScale = new Vector3(1f, 1f, 1f);
            obj.setResize();
            
            // sprRender.sortingOrder = 100;
            // boxColider.size = new Vector2(134.0f, 134.0f);

        }
    }

    public Gem getGem(int x, int y)
    {
        if (x < 0 || x >= mWidth)
            return null;

        if (y < 0 || y >= mHeight)
            return null;

      return arrayMap[x, y].GetComponent<Gem>();
    }
}