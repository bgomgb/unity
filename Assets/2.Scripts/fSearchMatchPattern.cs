﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class fSearchMatchPattern {

    public List<Gem> RemoveGem = new List<Gem>();
    public void SearchPattern(Gem gem)
    {
        RemoveGem.Clear();

        CheckMatchHorizon(gem, GameDefine.MatchCount);
        CheckMatchVetical(gem, GameDefine.MatchCount);


        if(RemoveGem.Count!= 0)
        {
            Debug.Log("값있다");
        }
    }

    private bool CheckMatchHorizon(Gem gem,int MatchCount)
    {
        List<Gem> ListMatchHorizon = new  List<Gem>();
        int CheckPointX =  MatchCount*-1  + 1;

        for (int i = 0; i < MatchCount * 2 - 1; i++)
        {
            int iX = gem.X + CheckPointX;
            Gem checkGem = StageManager.Instance.getGem(iX, gem.Y);

            if (checkGem == null || checkGem.mCode!= gem.mCode)
            {
                ListMatchHorizon.Clear();
            }
            else
            {
                ListMatchHorizon.Add(checkGem);
            }

               ++CheckPointX;
        }

        if (ListMatchHorizon.Count < MatchCount)
            ListMatchHorizon.Clear();
        else
        {
            foreach(var member in ListMatchHorizon)
                RemoveGem.Add(member);
        }
            

       return true;
    }

    private bool CheckMatchVetical(Gem gem,int MatchCount)
    {
        List<Gem> ListMatchVetical = new List<Gem>();
        int CheckPointY = MatchCount * -1 + 1;

        for (int i = 0; i < MatchCount * 2 - 1; i++)
        {
            int iX = gem.X + CheckPointY;
            Gem checkGem = StageManager.Instance.getGem(iX, gem.Y);

            if (checkGem == null || checkGem.mCode != gem.mCode)
            {
                ListMatchVetical.Clear();
            }
            else
            {
                ListMatchVetical.Add(checkGem);
            }

            ++CheckPointY;
        }

        if (ListMatchVetical.Count < MatchCount)
            ListMatchVetical.Clear();
        else
        {
            foreach (var member in ListMatchVetical)
                RemoveGem.Add(member);
        }


        return true;
    }


}