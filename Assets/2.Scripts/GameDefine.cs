﻿using UnityEngine;
using UnityEditor;

static public class GameDefine 
{
    public enum GemGode
    {
        None       = 0,
        Gem_Blue   = 1,
        Gem_Green  = 2,
        Gem_Orange = 3,
        Gem_Purple = 4,
        Gem_Red    = 5,
        Gem_White  = 6,
        Gem_Yellow = 7,
        Count      = 8,
    }

    public const int MatchCount = 3;
}