﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;


public class Drag : MonoBehaviour ,IBeginDragHandler,IEndDragHandler, IDragHandler,IPointerEnterHandler
{
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");

        if (StageManager.Instance.SelectGem != null)
            return;

        if(MovedGem.Instance.bIsMoveGem == false)
            StageManager.Instance.SelectGem = this.GetComponent<Gem>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");

        //MovedGem.Instance.MovedGems(StageManager.Instance.SelectGem, Cgem);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        StageManager.Instance.SelectGem = null;
        //Debug.Log("OnEndDrag");

        //StageManager.Instance.SelectGem = null;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Gem Cgem = this.GetComponent<Gem>();

        if (Cgem != null && StageManager.Instance.SelectGem != null)
        {

            if (!StageManager.Instance.SelectGem.checkMoveGem(Cgem))
                return;

            if(StageManager.Instance.SelectGem != null &&MovedGem.Instance.bIsMoveGem == false&& StageManager.Instance.SelectGem.mCode != Cgem.mCode)
            {
                StartCoroutine(StageManager.Instance.gamesystem.GemMove(Cgem));
            }
        }
    }

}

