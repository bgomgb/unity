﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeUI : MonoBehaviour
{
    // Start is called before the first frame update
    public float playtime;
    public Text timeText;
    void Start()
    {
        playtime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        playtime += Time.deltaTime;

        int min = (int)playtime / 60;
        int sec = (int)((playtime) - min * 60);

        timeText.text =  min.ToString("00") + ":"+sec.ToString("00");
    }
}
